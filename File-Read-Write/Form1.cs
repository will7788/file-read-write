﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace File_Read_Write
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void ChooseFile_Click(object sender, EventArgs e)
        {
            StreamWriter outputFile;
            StreamReader inputFile;
            string currentLine;
            string longestString = "";
            string mostVowel = "";
            string first = "";
            string last = "";
            List<string> fileList = new List<string>();
            int longest = 0;
            int vowelCount;
            int currentChamp = 0;

            if (userFile.ShowDialog() == DialogResult.OK) {
                inputFile = File.OpenText(userFile.FileName);
                while (!inputFile.EndOfStream)
                {
                    //Find longest string
                    currentLine = inputFile.ReadLine().ToLower();
                    fileList.Add(currentLine);
                    if (currentLine.Length > longest)
                    {
                        longestString = currentLine;
                    }
                    //Find most vowels
                    vowelCount = 0;
                    foreach (char c in currentLine)
                    {
                        if (c.Equals("a") || c.Equals("e") || c.Equals("i") || c.Equals("o") || c.Equals("u"))
                        {
                            vowelCount++;
                        }
                        if (vowelCount > currentChamp)
                        {
                            currentChamp = vowelCount;
                            mostVowel = currentLine;
                        }
                    }
                }
                fileList.Sort();
                outputBox.AppendText("Longest word: " + longestString);
                outputBox.AppendText(Environment.NewLine);
                outputBox.AppendText("Most Vowels: " + mostVowel);
                outputBox.AppendText(Environment.NewLine);
                outputBox.AppendText("Alphabetical (first): " + fileList.First());
                outputBox.AppendText(Environment.NewLine);
                outputBox.AppendText("Alphabetical (last): " + fileList.Last());


            }

            else {
                MessageBox.Show("You clicked the Cancel button.");
            }


        }
    }
}
